/**
 * Berikan jawabanmu dibawah ini!
 */

const Arr = [
    {
        'name': 'Avanza',
        'color': 'Black',
        'price': 1000000,
    },
    {
        'name': 'Rush',
        'color': 'White',
        'price': 2000000,
    },
    {
        'name': 'Kijang',
        'color': 'Brown',
        'price': 5000000,
    },
    {
        'name': 'Jazz',
        'color': 'Silver',
        'price': 5000000,
    },
    {
        'name': 'Karimun',
        'color': 'Red',
        'price': 50000,
    }
]

console.log(Arr);
