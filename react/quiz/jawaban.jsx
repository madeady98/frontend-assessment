import React, { useState } from "react";
import './jawaban.css';

const Quiz = () => {

    const fieldProduct = {
        id: '',
        name: '',
        qty: '',
    };
    const [data, setData] = useState([])
    const [edit, setEdit] = useState({})
    const [product, setProduct] = useState(fieldProduct)

    function generateId() {
        return Date.now()
    }

    const handleChange = (e) => {
        setProduct(
            {
                ...product,
                [e.target.name]: e.target.value
            })
    }

    let { name, qty } = product
    const handlerSubmit = (e) => {
        e.preventDefault();
        if (product.name === '' || product.qty === '') {
            return window.alert('Field Product & Qty must be filled');
        }

        if (edit.id) {
            const updatedProduct = {
                ...edit,
                name,
                qty
            }
            const getIndex = data.findIndex(function (item) {
                return item.id == edit.id
            })
            const updatedProducts = [...data]
            updatedProducts[getIndex] = updatedProduct

            window.alert(`${product.name} updated successfully`);
            setEdit(fieldProduct)
            setProduct(fieldProduct)
            return setData(updatedProducts)
        }

        setData([
            ...data,
            {
                id: generateId(),
                name,
                qty
            }
        ])
        window.alert(`${product.name} created successfully`);
        setProduct(fieldProduct)
    }

    const handlerEdit = (item) => {
        setProduct({
            'id': item.id,
            'name': item.name,
            'qty': item.qty
        })
        setEdit(item)
    }

    const handlerDel = (itemId) => {
        const messageDelete = window.confirm(`Are You Sure to Delete ${product.name}`)
        if (messageDelete) {
            const filterData = data.filter(function (item) {
                return item.id !== itemId
            })
            window.alert(`${product.name} deleted successfully`);
            setData(filterData)
        }

    }
    return (
        <div className="wrapper">
            <div className="form-box">
                <h1>Shopping List</h1>
                <form action="" onSubmit={handlerSubmit}>
                    <div className="inputProduct">
                        <label htmlFor="">Product</label>
                        <input
                            type="text"
                            name="name"
                            value={product.name}
                            onChange={handleChange}
                        />
                    </div>
                    <div className="inputProduct">
                        <label htmlFor="">Qty</label>
                        <input
                            type="text"
                            name="qty"
                            value={product.qty}
                            onChange={handleChange}
                        />
                    </div>
                    <div className="boxButton">
                        <button type="submit">{edit.id ? 'Update' : 'Submit'}</button>
                    </div>
                </form>
            </div>
            <div className="table-box">
                <table className="table-product">
                    <thead>
                        <tr>
                            <th style={{ width: '70%' }}>Product</th>
                            <th>Qty</th>
                            <th style={{ width: '30%' }} >Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {data.length > 0 ? (
                            <>
                                {
                                    data.map(function (item) {
                                        return (
                                            <tr key={item.id}>
                                                <td>{item.name}</td>
                                                <td style={{ textAlign: 'right' }} >{item.qty}</td>
                                                <td className="button-action" >
                                                    <button onClick={handlerEdit.bind(this, item)}>Edit</button>
                                                    <button onClick={handlerDel.bind(this, item.id)} >Delete</button>
                                                </td>
                                            </tr>
                                        )
                                    })
                                }
                            </>
                        ) : (
                            <tr>
                                <td colSpan={3} style={{ textAlign: 'center' }} >List Empty</td>
                            </tr>
                        )}
                    </tbody>
                </table>
            </div>
    </div>
  );
};





export default Quiz;
